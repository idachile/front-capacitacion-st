(function(window, document, $){
    "use strict";

    var DEBUG = true;

    var $window = $(window),
        $document = $(document),
        $body, $mainNav, $mainHeader;

    /// guardo los media queries
    var TABLETS_DOWN = 'screen and (max-width: 1024px)',
        VERTICAL_TABLETS_DOWN = 'screen and (max-width: 850px)',
        VERTICAL_TABLETS_UP = 'screen and (min-width: 850px)',
        SMALLTABLET_DOWN = 'screen and (max-width: 768px)',
        PHABLET_DOWN = 'screen and (max-width: 640px)',
        PHONE_DOWN = 'screen and (max-width: 480px)';

    var throttle = function( fn ){
        return setTimeout(fn, 1);
    };

    var mqMap = function( mq ){
        var MQ = '';

        switch( mq ){
            case 'tablet-down' :
                MQ = TABLETS_DOWN;
                break;
            case 'vertical-tablet-down' :
                MQ = VERTICAL_TABLETS_DOWN;
                break;
            case 'smalltablet-down' :
                MQ = SMALLTABLET_DOWN;
                break;
            case 'phablet-down' :
                MQ = PHABLET_DOWN;
                break;
             case 'phone-down' :
                MQ = PHONE_DOWN;
                break;
        }

        return MQ;
    };

    var normalize = (function() {
            var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç", 
                to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
                mapping = {};

            for(var i = 0, j = from.length; i < j; i++ )
                mapping[ from.charAt( i ) ] = to.charAt( i );

            return function( str ) {
                var ret = [];
                for( var i = 0, j = str.length; i < j; i++ ) {
                    var c = str.charAt( i );
                    if( mapping.hasOwnProperty( str.charAt( i ) ) )
                        ret.push( mapping[ c ] );
                    else
                        ret.push( c );
                }
                return ret.join( '' );
            };
        })();


    // trackeos de analytics
    var track = function( data ){
        if( !dataLayer ){
            DEBUG && console.log('No hay dataLayer');
            return;
        }

        dataLayer.push(data);
        DEBUG && console.log('Enviado a GTM', data);
    };

    var App = function(){
        this.path = document.body.getAttribute('data-path');
        this.ajaxURL = '/wp-admin/admin-ajax.php';
        this.loadLegacyAssets();
    };

    App.prototype = {
        onReady : function(){
            this.setGlobals();
            this.autoHandleEvents( $('[data-func]') );
            this.handleMobileTables();
            this.conditionalInits();
            this.initTextCounter();
            this.scrollFixed();
            this.handleForms();

            // elimina el 300ms delay en IOS
            $('a, button, input[type="submit"]').on('touchstart', $.noop);

            // activa trackeos de eventos en GTM
            $body.on('click.st_gtm', '[data-track]', function(){
                var $item = $(this);

                track({
                    event : $item.data('gtm-event'),
                    eventCategory : $item.data('gtm-eventcategory'),
                    eventAction : $item.data('gtm-eventaction'),
                    eventLabel : $item.data('gtm-eventlabel')
                });
            });
        },
        
        onLoad : function(){
            var app = this;

            $('[data-equalize="children"][data-mq="tablet-down"]').equalizeChildrenHeights(true, TABLETS_DOWN);
            $('[data-equalize="children"][data-mq="vertical-tablet-down"]').equalizeChildrenHeights(true, VERTICAL_TABLETS_DOWN);
            $('[data-equalize="target"][data-mq="vertical-tablet-down"]').equalizeTarget(true, VERTICAL_TABLETS_DOWN);
            $('[data-equalize="target"][data-mq="vertical-tablet-up"]').equalizeTarget(true, VERTICAL_TABLETS_UP);
            $('[data-equalize="target"][data-mq="smalltablet-down"]').equalizeTarget(true, SMALLTABLET_DOWN);
            $('[data-equalize="target"][data-mq="phablet-down"]').equalizeTarget(true, PHABLET_DOWN);
            $('[data-equalize="target"][data-mq="phone-down"]').equalizeTarget(true, PHONE_DOWN);

            // malditos disenadores y sus "ideas"
            $('.menu-principal > li > a, .inner-main-menu > li > a').equalizeHeights(true, VERTICAL_TABLETS_DOWN);

            // videos elasticos
            $('.experiencia-destacada .thumbnail, .experiencia .thumbnail').fitVids();

             //  se activa masonry
            if( document.querySelector('[data-masonry-grid]') ){
                throttle(function(){
                    app.masonry = new Masonry( document.querySelector('[data-masonry-grid]'), {
                        itemSelector: '.masonry-item',
                        percentPosition: true
                    });
                });
            }

            app.setFixedHeader();
            $window.trigger('resize');
        },

        onResize : function(){
            var app = this;
            throttle(function(){
                app.setFixedHeader();
            });
        },

        onScroll : function(){},
        
        loadLegacyAssets : function(){
            // voy a asumir que cualquier browser que no soporte <canvas> es un oldIE (IE8-)
            if( Modernizr.canvas ){ return false; }

            Modernizr.load({
                load : this.path + 'scripts/support/selectivizr.min.js'
            });
        },
        
        autoHandleEvents : function( $elements ){
            if( !$elements || !$elements.length ){ return false; }

            var self = this;

            $elements.each(function(i,el){
                var func = el.getAttribute('data-func') || false,
                    evts = el.getAttribute('data-events') || 'click.customStuff';

                if( func && typeof( self[func] ) === 'function' ){
                    $(el)
                        .off(evts)
                        .on(evts, $.proxy(self[func], self))
                        .attr('data-delegated', 'true');
                }
            });
        },

        setEnquire : function(){
            var app = this,
                $mutable = $('[data-mutable]');

            enquire.register( TABLETS_DOWN, [{
                match: function(){
                    app.moveElements($mutable.filter('[data-mutable="tablet-down"]'), 'mobile');
                },
                unmatch: function(){
                    app.moveElements($mutable.filter('[data-mutable="tablet-down"]'), 'desktop');
                }
            }]);

            enquire.register( SMALLTABLET_DOWN, [{
                match: function(){
                    app.moveElements($mutable.filter('[data-mutable="smalltablet-down"]'), 'mobile');
                },
                unmatch: function(){
                    app.moveElements($mutable.filter('[data-mutable="smalltablet-down"]'), 'desktop');
                }
            }]);

            enquire.register( PHONE_DOWN, [{
                match: function(){
                    app.moveElements($mutable.filter('[data-mutable="phone-down"]'), 'mobile');
                },
                unmatch: function(){
                    app.moveElements($mutable.filter('[data-mutable="phone-down"]'), 'desktop');
                }
            }]);

            enquire.register( VERTICAL_TABLETS_DOWN, [{
                match: function(){
                    app.moveElements($mutable.filter('[data-mutable="vertical-tablet-down"]'), 'mobile');
                },
                unmatch: function(){
                    app.moveElements($mutable.filter('[data-mutable="vertical-tablet-down"]'), 'desktop');

                    // en caso de que el menu este desplegado cuando se hace un resize
                    $('[data-func="deployMainNav"]').removeClass('deployed');
                    $('#main-nav').removeClass('deployed').removeAttr('style');
                }
            }]);

            // para qeu todo funcione bien en movil
            // se gatilla el evento resize en window
            $window.trigger('resize');
        },

        conditionalInits : function(){
            // modulo de sliders en singles
            if( $('[data-role="single-slider-module"]').length ){
                this.singleSlider( $('[data-role="single-slider-module"]') );
            }

            // modulo de sliders en home
            if( $('[data-role="main-slider"]').length ){
                this.mainSlider( $('[data-role="main-slider"]') );
            }

            // modulo de sliders en home
            if( $('[data-role="holy-slider"]').length ){
                this.holySlider( $('[data-role="holy-slider"]') );
            }

            /// para los single que tengan contadores de shares
            if( $('[data-role="share-counter"]').length ){
                this.getShareCount( $('[data-role="share-counter"]') );
            }

            /// todos los inpiut type file se estilizan
            if( $('input[type="file"]').length ){
                $('input[type="file"]').beautify();
            }

            // mapas de google
            if( typeof google !== 'undefined' && $('[data-googlemap]').length ){
                this.handleMaps( $('[data-googlemap]') );
            }

            // si no reconoces matchmedia no mereces enquire
            if( window.matchMedia ){
                this.setEnquire();
            }

            /// cuando se encuentra touch el menu actua diferente
            if( Modernizr.touch ){
                this.touchSubmenus();
            }

            // si hay inner nav entonces se maneja 
            if( document.querySelector('#inner-nav') && Modernizr.touch ){
                $('.inner-main-menu').on('click', '.section-title > a', function( event ){
                    event.stopPropagation();
                    event.preventDefault();

                    var $li = $(event.currentTarget).parent();
                    $li.parents('.inner-main-menu').toggleClass('deployed');
                });

                $document.on('click', function(){
                    $('.inner-main-menu').removeClass('deployed');
                });
            }
        },

        setGlobals : function(){
            $body = $('body');
            $mainHeader = $('#main-header');
            $mainNav = $('#main-nav');

            if( $mainNav.length ){
                this.navPos = $mainNav.offset().top;
            }
        },

        ///////////////////////////////////////////////////////////
        /////////////////////////////// Auxiliares
        ///////////////////////////////////////////////////////////
        debug : function( message ){
            DEBUG && console.log( message );
        },

        moveElements : function( $set, type ){
            var areaType = 'data-' + type +'-area',
                groups = $set.groupByAtt( areaType );

            groups.forEach(function( $group ){
                var $target = $('[data-area-name="'+ $group.first().attr( areaType ) +'"]');

                $group.sort(function(a, b){
                    return $(a).data('order') - $(b).data('order');
                });

                $group.appendTo( $target );
            });
        },

        genericInvalidInputAction : function( $input ){
            // informacion basica
            var offset = $input.offset(),
                height = $input.outerHeight(),
                width = $input.outerWidth(),
                id = $input.attr('id'),
                mensaje = $input.data('error-message') || 'Debe llenar el campo para continuar';

            if( ! $('#' + id + '-error-tooltip').length ){
                // genero el tooltip
                var $tooltip = $('<div />').attr({
                        'id' : id + '-error-tooltip',
                        'class' : 'form-error-tooltip'
                    }).text( mensaje );

                $tooltip.appendTo('body').css({
                    top : offset.top + height + 10,
                    left : offset.left + width - $tooltip.outerWidth()
                });

                setTimeout(function(){
                    $tooltip.fadeOut(500, function(){ $tooltip.remove(); });
                }, 600);
            }
        },

        handleForms : function(){
            var app = this;

            /// formularios basicos
            /// busqueda
            /// contacto
            /// newsletter
            /// enviar por mail
            $('[data-validation="basic"]').commonForm({
                delegate_keyup : false,
                notValidInputCallback : this.genericInvalidInputAction
            });
        },

        setFixedHeader : function(){
            var header = document.querySelector('#header-body');
            var inner_menu = document.querySelector('#inner-nav');
            var finalMargin = 0;

            if( !header ){ return; }

            if( Modernizr.mq(VERTICAL_TABLETS_DOWN) ){
                var innerMenuHeight = 0;
                if( inner_menu ){ innerMenuHeight = inner_menu.offsetHeight; }

                finalMargin = (header.offsetHeight + innerMenuHeight) + 'px';
            }
            document.body.style.marginTop = finalMargin;
        },

        touchSubmenus : function(){
            $('.menu-principal, .menu-principal-collapsed').on('click', '.menu-item-has-children > a', function( event ){
                event.stopPropagation();

                var $li = $(event.currentTarget).parent();

                if( $li.hasClass('deployed') ){
                    return true;
                }

                event.preventDefault();

                $li.siblings().removeClass('deployed');
                $li.addClass('deployed');
            });

            $document.on('click', function(){
                $('.menu-principal .menu-item-has-children, .menu-principal-collapsed .menu-item-has-children').removeClass('deployed');
            });
        },

        handleMobileTables : function(){
            $('.page-content table').each(function(i, table){
                $(table).wrap('<div class="regular-content-table-holder"></div>');
            });
        },

        setLightBox : function( classes ){
            /// se crean los elementos
            var $bg = $('<div />').attr({ id : 'lightbox-background', class : 'lightbox-background' }),
                $scrollable = $('<div />').attr({ class : 'lightbox-scrollable-holder' }),
                $holder = $('<div />').attr({ class : 'lighbox-holder' }).append('<div class="lightbox-close-holder"></div>'),
                $content = $('<div />').attr({ class : 'lightbox-content' }),
                $closeBtn = $('<button class="primary-btn small-btn icon-btn--close-lb"></button>');

            // se inicia la promesa
            var promise = new $.Deferred();

            if( classes ){
                $holder.addClass( classes );
            }
            $closeBtn.on('click', this.closeLightBox);

            $holder.appendTo( $scrollable ).find('.lightbox-close-holder').append( $closeBtn );

            $body.append( $bg );

            $bg.animate({ opacity : 1 }).promise().then(function(){
                $body.css('overflow', 'hidden');
                $bg.append( $scrollable );
                $holder.append( $content );
                promise.resolve( $bg, $content );
            });

            return promise;
        },

        closeLightBox : function(){
            $('#lightbox-background').remove();
            $body.css('overflow', 'auto');
            $('<button class="primary-btn small-btn icon-btn--close-lb"></button>').unbind('click');
        },

        getShareCount : function( $elements ){
            $elements.each(function(index, element){
                var type = element.getAttribute('data-type'),
                    url = element.getAttribute('data-url') || window.location.href,
                    jsonUrl = '',
                    data = {};

                var params = {
                    nolog: true,
                    id: url,
                    source: "widget",
                    userId: "@viewer",
                    groupId: "@self"
                };

                if( type === 'facebook' ){
                    jsonUrl = 'http://graph.facebook.com/';
                    data.id = url;
                }
                else if( type === 'twitter' ){
                    jsonUrl = 'http://cdn.api.twitter.com/1/urls/count.json';
                    data.url = url;
                }
                else if( type === 'linkedin' ){
                    jsonUrl = 'http://www.linkedin.com/countserv/count/share';
                    data.url = url;
                }

                $.ajax({
                    method : 'GET',
                    url : jsonUrl,
                    data : data,
                    dataType : 'jsonp'
                }).then(function( response ){
                    var count = '';

                    // se saca el valor de cada red segun lo que responda el API correspondiente
                    if( type === 'facebook' ){ count = response.shares; }
                    else if( type === 'twitter' ){ count = response.count; }
                    else if( type === 'linkedin' ){ count = response.count; }

                    // prevencion de error en caso de false o undefined
                    count = count ? count : 0;
                    element.textContent = count;
                });
            });
        },

        handleMaps : function( $boxes ){
            $boxes.ninjaMap();
        },

        ///////////////////////////////////////////////////////////
        /////////////////////////////// Modulos
        ///////////////////////////////////////////////////////////
        singleSlider : function( $elements ){
            /// primero revisamos la dependencia en ninjaSlider
            if( typeof window.NinjaSlider === 'undefined' || typeof $.fn.owlCarousel === 'undefined' ){
                this.debug('Falta ninjaSlider');
                this.debug('Falta owlCarousel');
                return false;
            }

            var app = this;

            $elements.each(function(i, module){
                var $module = $(module),
                    $slider = $module.find('[data-role="slider"]'),
                    $arrows = $module.find('[data-role="single-slider-arrow"]'),
                    $thumbnailsHolder = $module.find('[data-role="thumbnails-holder"]'),
                    $thumbnailArrows = $module.find('[data-role="single-slider-thumbnail-arrow"]'),
                    $thumbnails = $module.find('[data-role="single-slider-thumbnail"]'),
                    slider, carousel; 

                // se inicializa el carousel de los thumbnails-holder
                carousel = $thumbnailsHolder.owlCarousel({ 
                    items : 6,
                    itemsDesktop : [1199,4],
                    itemsDesktopSmall : [980,4],
                    itemsTablet: [768,3],
                    itemsMobile : [320,2],
                }).data('owlCarousel');

                // se inicializa el slider principal
                slider = $slider.ninjaSlider({
                    auto : false,
                    transitionCallback : function(index, activeSlide, container){
                        var $slide = $(activeSlide);
                        $slide.siblings().removeClass('active');
                        $slide.addClass('active');

                        $thumbnails.removeClass('active').filter('[data-target="'+ index +'"]').addClass('active');
                        carousel.goTo( index );
                    }
                }).data('ninjaSlider');

                // se delegan las flechas principales
                $arrows.on('click.singleSlider', function(e){
                    // this es el boton 
                    e.preventDefault();

                    var direction = this.getAttribute('data-direction');
                    slider[ direction ]();
                });

                // se delegan los thumbnails para que actuen sobre el slider principal
                $thumbnails.on('click.singleSlider', function(e){
                    // this es el boton 
                    e.preventDefault();

                    var target = parseInt( this.getAttribute('data-target') );
                    slider.slide( target );
                });

                // se deben setear controles de los thumbnails
                $thumbnailArrows.on('click.singleSlider', function(e){
                    // this es el boton 
                    e.preventDefault();

                    var direction = this.getAttribute('data-direction');
                    carousel[ direction ]();
                });

                // solo se deben delegar las teclas si hay un solo slider
                if( $elements.length === 1 ){
                    $window.on('keyup.singleSlider', function( e ){
                        if( e.keyCode == 39 ){ slider.next(); }
                        else if( e.keyCode == 37 ){ slider.prev(); }
                    });
                }
            });
        },

        mainSlider : function( $element ){
            /// primero revisamos la dependencia en ninjaSlider
            if( typeof window.NinjaSlider === 'undefined'){
                this.debug('Falta ninjaSlider');
                return false;
            }

            var $slider = $element,
                $bullets = $slider.find('[data-role="slider-bullet"]'),
                $transition = $slider.data('transition'),
                $auto,
                ninjaSlider;
            if(typeof $transition !== 'undefined'){
                $auto = $transition;
            }else{
                $auto = 6000;
            }
            // se inicializa el slider principal
            ninjaSlider = $slider.ninjaSlider({
                auto : $auto,
                transitionCallback : function(index, activeSlide, container){
                    var $slide = $(activeSlide);
                    $slide.siblings().removeClass('current');
                    $slide.addClass('current');

                    $bullets.removeClass('current').filter('[data-index="'+ index +'"]').addClass('current');
                }
            }).data('ninjaSlider');

            $bullets.on('click.mainSlider', function( ev ){
                ev.preventDefault();
                ninjaSlider.slide( $(ev.currentTarget).data('index') );
            });
        },

        holySlider : function( $element ){
            /// primero revisamos la dependencia en ninjaSlider
            if( typeof window.NinjaSlider === 'undefined'){
                this.debug('Falta ninjaSlider');
                return false;
            }

            var $slider = $element,
                $bullets = $slider.find('[data-role="slider-bullet"]'),
                $arrows = $slider.find('[data-role="single-slider-arrow"]'),
                $transition = $slider.data('transition'),
                $auto,
                ninjaSlider;
            if(typeof $transition !== 'undefined'){
                $auto = $transition;
            }else{
                $auto = 6000;
            }
            // se inicializa el slider principal
            ninjaSlider = $slider.ninjaSlider({
                auto : $auto,
                transitionCallback : function(index, activeSlide, container){
                    var $slide = $(activeSlide);
                    $slide.siblings().removeClass('current');
                    $slide.addClass('current');

                    $bullets.removeClass('current').filter('[data-index="'+ index +'"]').addClass('current');
                }
            }).data('ninjaSlider');

            // se delegan las flechas principales
            $arrows.on('click.holySlider', function(e){
                // this es el boton 
                e.preventDefault();

                var direction = this.getAttribute('data-direction');
                ninjaSlider[ direction ]();
            });

            $bullets.on('click.holySlider', function( ev ){
                ev.preventDefault();
                ninjaSlider.slide( $(ev.currentTarget).data('index') );
            });
        },

        ///////////////////////////////////////////////////////////
        /////////////////////////////// Delegaciones directas
        ///////////////////////////////////////////////////////////
        deployeMobileNav : function( event ){
            event.preventDefault();
            var $btn = $(event.currentTarget),
                $main_nav = $('#mobile-nav'),
                $header = $('#header-body');

            if( $main_nav.hasClass('deployed') ){
                $btn.removeClass('deployed');
                $main_nav.removeClass('deployed');
                $body.removeClass('nav-deployed');

                // acciones seguidas para seguir animacion en css
                $main_nav.css({'max-height' : 0});
            }
            else {
                $btn.addClass('deployed');
                $body.addClass('nav-deployed');

                // acciones seguidas para seguir animacion en css
                $main_nav.css({
                    'max-height' : $window.height() - $header.height()
                });

                setTimeout(function(){
                    $main_nav.addClass('deployed');
                }, 500);
            }
        },

        toggleTarget : function( event ){
            event.preventDefault();

            // se revisa si esta limitado a un media query
            if( event.currentTarget.getAttribute('data-mq') ){
                var mqString = mqMap( event.currentTarget.getAttribute('data-mq') );

                // se revisa si entra al media query especificado
                if( mqString && !Modernizr.mq( mqString ) ){ return; }
            }

            // se selecciona a traves del atributo data-target
            $( event.currentTarget.getAttribute('data-target') ).toggleClass('deployed');

            // expansion para cuando quiero enfocar algo despues de mostrarlo
            if( event.currentTarget.getAttribute('data-focus') ){
                $( event.currentTarget.getAttribute('data-focus') ).focus();
            }
        },

        collapseControl : function( event ){
            event.preventDefault();
            var $button = $(event.currentTarget),
                $target = $('[data-target-name="'+ $button.data('target') +'"]'),
                $targets = $('[data-body]');
                
            $target.toggleClass('active', function(){
                $targets.removeClass('active');
            });
            $button.toggleClass('active', 500);
        },

        tabControl : function( event ){
            event.preventDefault();

            var $button = $(event.currentTarget),
                $target = $('[data-tab-name="'+ $button.data('target') +'"]');

            $button.siblings().removeClass('active');
            $target.siblings().removeClass('active');

            // si estoy clickeando el activo
            // en moviles se usa para deplegar el menu
            if( $button.hasClass('active') ){
                $button.parents('.tabs-controls').toggleClass('deployed');
                return;
            }

            $target.addClass('active');

            throttle(function(){
                $button.addClass('active');
                
                $button.parents('.tabs-controls').removeClass('deployed');
                $window.trigger('resize');
            });
        },

        deployParent : function( event ){
            event.preventDefault();

            // se revisa si esta limitado a un media query
            if( event.currentTarget.getAttribute('data-mq') ){
                var mqString = mqMap( event.currentTarget.getAttribute('data-mq') );

                // se revisa si entra al media query especificado
                if( mqString && !Modernizr.mq( mqString ) ){ return; }
            }

            $(event.currentTarget).parents( event.currentTarget.getAttribute('data-parent') ).toggleClass('deployed');

            if( typeof this.masonry !== 'undefined' ){
                this.masonry.layout();
            }
        },

        showShortUrl : function( event ){
            event.preventDefault();
            event.stopPropagation();

            var self = this,
                $item = $(event.currentTarget),
                shortUrl = $item.data('link') || $('link[rel="shortlink"]').attr('href') || window.location.href,
                urlInput = $('<input class="tooltip-data-input" type="text" name="short-url" value="'+ shortUrl +'" readonly>').get(0),
                $tooltip = $('<div />').attr({
                    'id' : 'short-url-tooltip-object',
                    'class' : 'regular-tooltip short-url'
                }).append( urlInput ),
                position = $item.offset(),
                unloadFunc = function( e ){
                    $('#short-url-tooltip-object').remove();
                    $(this).off('click.tooltip');
                };

            // primero se saca cualquiera que actualmente se este mostrando
            $('#short-url-tooltip-object').remove();

            // se setean las propiedades y se adjunta al body
            $tooltip.appendTo('body').css({
                'position' : 'absolute',
                'top' : position.top - $tooltip.outerHeight() - 20,
                'left' : position.left - $tooltip.outerWidth() + $item.outerWidth(),
                'opacity' : 1
            }).on('click', function(e){
                e.stopPropagation();
            });

            urlInput.setSelectionRange(0, urlInput.value.length);

            $('body').on('click.tooltip', unloadFunc);
        },

        printPage : function( event ){
            event.preventDefault();
            window.print();
        },

        expandFootNote : function( event ){
            event.preventDefault();
            $( event.currentTarget ).toggleClass('expanded');
        },

        inputControl : function( event ){
            var $item = $(event.currentTarget);

            if( ($item.is('[type="radio"]') && $item.is(':checked')) || $item.is('select') ){
                $('[data-role="'+ $item.data('group') +'"]')
                    .removeClass('active')
                    .find('input, select, textarea')
                    .removeAttr('required');

                $('[data-role="'+ $item.data('group') +'"][data-name="'+ $item.val() +'"]')
                    .addClass('active')
                    .find('input, select, textarea')
                    .attr('required', true);
            }
        },

        goToTop : function( event ){
            event.preventDefault();
            $('html, body').animate({scrollTop : 0},800);
        },

        scrollToTarget : function( event ){
            event.preventDefault();
            event.stopPropagation();
            var targetPos = $( event.currentTarget.getAttribute('href') ).offset().top,
                offset = parseInt( event.currentTarget.getAttribute('data-offset') ),
                $parent = $(event.currentTarget).parent();

            if( offset ){
                targetPos = targetPos - offset;
                //added
                $parent.siblings().removeClass('menu-item--current');
                $parent.addClass('menu-item--current');
            }

            $('html, body').animate({ scrollTop : targetPos }, 500);
        },

        deployFullGallery : function( event ){
            event.preventDefault();

            var app = this,
                $item = $(event.currentTarget),
                lightbox_promise = this.setLightBox('gallery-detail'),
                ajax_promise = $.get(this.ajaxURL, {
                    action : 'st_front_ajax',
                    funcion : 'deploy_full_gallery',
                    pid : $item.data('id')
                });

            $.when(lightbox_promise, ajax_promise).then(function( lightbox_info, ajax_response ){
                var $lightbox_bg = lightbox_info[0],
                    $lightbox_content = lightbox_info[1],
                    response = ajax_response[0];

                $lightbox_content.append( response );

                if( $('[data-role="single-slider-module"]').length ){
                    app.singleSlider( $lightbox_content.find('[data-role="single-slider-module"]') );
                    var slider = $lightbox_content.find('[data-role="slider"]').data('ninjaSlider');

                    // se mueve el slider al elemento que se clickeo
                    slider.slide( $item.data('target-index') );
                }

                throttle(function(){
                    $lightbox_bg.addClass('loaded');
                });


            }).fail(function(){
                app.debug('fallo algo en el ajax');
                app.closeLightBox();
            });
        },

        loadModal : function( event ){
            event.preventDefault();

            var app = this,
                $item = $(event.currentTarget),
                lightbox_promise = this.setLightBox('programa-detail'),
                ajax_promise = $.ajax({
					url: $item.data('target'),
					method: $item.data('method') || 'GET',
					data: $item.data('query') || {}
				});
                
            $.when(lightbox_promise, ajax_promise).then(function( lightbox_info, ajax_response ){
                var $lightbox_bg = lightbox_info[0],
                    $lightbox_content = lightbox_info[1],
                    response = ajax_response[0];

                $lightbox_content.append( response );

                throttle(function(){
                    $lightbox_bg.addClass('loaded');
                });

            }).fail(function(){
                app.debug('fallo algo en el ajax');
                app.closeLightBox();
            });
        },

        loadTemaSello : function( event ){
            event.preventDefault();

            var app = this,
                $button = $(event.currentTarget),
                $thumb_box = $('[data-role="tema-thumbnail"]'),
                $content_box = $('[data-role="tema-content"]');

            // se indica la carga de informacion en ambas cajas
            $thumb_box.animate({opacity: .2}, 300);
            $content_box.animate({opacity: .2}, 300);

            $.getJSON( this.ajaxURL, {
                action : 'st_front_ajax',
                funcion : 'load_tema_sello',
                pid : $button.data('pid')
            }).then(function( response ){
                var $new_content = $(response.content);

                // se reemplaza la imagen principal
                $thumb_box.html( response.thumbnail );
                $thumb_box.animate({opacity: 1}, 300);

                // se reemplaza el contenido
                $content_box.html( $new_content.html() );
                $content_box.animate({opacity: 1}, 300);

                app.autoHandleEvents( $content_box.find('[data-func]') );

                $button.parents('.page-nav').find('.current').removeClass('current');
                $button.addClass('current');

                // se tira una pagina virtual para indicar el cambio a GTM
                if( dataLayer ){
                    dataLayer.push({
                        'event' : 'VirtualPageview',
                        'virtualPageURL' : '/tema/'+ response.year,
                        'virtualPageTitle' : 'Tema sello año '+ response.year
                    });
                }
            });
        },

        cloneInputGroup : function( event ){
            event.preventDefault();

            var $btn = $(event.currentTarget),
                $groups = $('[data-role="clone"]'),
                $clone = $groups.first().clone(true),
                new_index = $groups.length;

            // se prepara el input cambiando los names y el index del clon
            $clone.find('input, select, textarea').each(function(i, input){
                var $input = $(input),
                    new_name = $input.data('group') + '['+ new_index +']' + '['+ $input.data('name') +']';
                $input.attr('name', new_name);

                // se resetean los valores del input
                $input.val('').prop('selectedIndex', 0);
            });

            $btn.before( $clone );

            if( $('[data-role="clone"]').length >= 3 ){ 
                $btn.hide();
            }
        },

        removeInputGroup : function( event ){
            event.preventDefault();

            var $btn = $(event.currentTarget);

            // primero matamos al grupo objetivo
            $btn.parents('[data-role="clone"]').remove();

            var $clones = $('[data-role="clone"]');

            if( $clones.length < 3 ){
                $('#clonador').show();
            }

            // despues se revisan y corrigen todos los names de todos los grupos restantes
            $clones.each(function(group_index, group){
                $(group).find('input, select, textarea').each(function(i, input){
                    var $input = $(input),
                        new_name = $input.data('group') + '['+ group_index +']' + '['+ $input.data('name') +']';
                    $input.attr('name', new_name);
                });
            });
        },
        //data-role="textcounter"
		//data-role="countdown"
		initTextCounter : function(){
			$('[data-textcounter]').keyup(function(e){
				var $textarea = $(this),
					maxlength = parseInt($textarea.attr('maxlength')),
					valuelength = $textarea.val().length,
					countdown = $textarea.parent().find('[data-role="countdown"]');
	
				e.preventDefault();
				countdown.text(maxlength - valuelength);

			});
        },
        
        calendarControl : function( event ){
            event.preventDefault();

            var app = this,
                $item = $(event.currentTarget),
                direction = $item.data('direction'),
                $dataHolder = $('[data-role="calendar-data"]'),
                month = $dataHolder.data('month'),
                year = $dataHolder.data('year'),
                $monthName = $('[data-role="calendar-month"]'),
                $itemsHolder = $('[data-role="calendar-items-holder"]');

            if( !app.isLoading ){
                app.isLoading = true;

                /// se debe indicar que se esta cargando, asumo estados estandar
                /// eso queire decir, opacity nomas
                $monthName.css('opacity', '0.2');
                $itemsHolder.css('opacity', '0.2');

                $.ajax({
                    method : 'get',
                    url : '/wp-json/cargar/calendario-actividades',
                    dataType : 'json',
                    data : {
                        month : month,
                        year : year,
                        direction : direction
                    }
                }).done(function( response ){
                    $dataHolder.data('month', response.month_num);
                    $dataHolder.data('year', response.year);

                    $monthName
                        .text( response.month_name + ' - ' +  response.year)
                        .attr({
                            'data-prev' : response.prev,
                            'data-next' : response.next
                        });

                    $itemsHolder.html( response.items );

                    $monthName.css('opacity', '1');
                    $itemsHolder.css('opacity', '1');

                    app.isLoading = false;
                });
            }
        },

        scrollFixed : function(){
            $('[data-social]').scroll(function(e){
                var y = $(this).scrollTop();
                console.log(y);
                if (y > 800) {
                    $('[data-sticky]').fadeIn();
                } else {
                    $('[data-sticky]').fadeOut();
                }
            });
        },
        showGallery : function( event ){
            event.preventDefault();

            var pid = event.currentTarget.getAttribute('data-pid');

            var lightbox_promise = this.setLightBox('gallery-detail'),
                ajax_promise = $.get('/wp-json/cargar/modal/' + pid);

            $.when(lightbox_promise, ajax_promise).then(function( lightbox_info, ajax_response ){
                var $lightbox_bg = lightbox_info[0],
                    $lightbox_content = lightbox_info[1],
                    response = ajax_response[0].html;

                $lightbox_content.append( response );
                
                throttle(function(){
                    $lightbox_bg.addClass('loaded');
                });

            }).fail(function(){
                app.debug('fallo algo en el ajax');
                app.closeLightBox();
            });
        },
    };

    var app = new App();

    $document.ready(function(){ app.onReady && app.onReady(); });

    $window.on({
        'load' : function(){ app.onLoad && app.onLoad(); },
        'resize' : function(){ app.onResize && app.onResize(); },
        'scroll' : function(){ app.onScroll && app.onScroll(); },
    });

}(this, this.document, jQuery));


/////////////////////////////////////////
// Plugins y APIS
/////////////////////////////////////////
(function( window, $, undefined ){
    var $window = $(window);

    // pruebas personalizadas para modernizr
    Modernizr.addTest('device', function(){
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    });


    var unique = function( arr ) {
        var unique = [], i;

        for (i = 0; i < arr.length; i++) {
            var current = arr[i];
            if (unique.indexOf(current) < 0) { unique.push(current); }
        }
        return unique;
    };

    $.fn.svgfallback = function( callback ) {
        if( Modernizr.svg ){ return false; }

        return this.each(function() {
            this.src = this.getAttribute('data-svgfallback');
        });
    };

    $.fn.groupByAtt = function( attname ){
        var $set = this,
            groups = [],
            posibles = [];

        // se guardan todos los posibles valores
        $set.each(function(i,el){
            posibles.push( el.getAttribute(attname) );
        });

        // se quitan los elementos duplicados dejando solo los unicos
        posibles = unique( posibles );

        // se itera sobre las posibilidades y se agrupan los elementos
        posibles.forEach(function( value ){
            groups.push($set.filter('['+ attname +'="'+ value +'"]'));
        });

        return groups;
    };

    $.fn.equalizeHeights = function( dinamic, mqException ){
        var items = this,
            eq_h = function( $collection ){
                var heightArray = [];

                $collection.removeClass('height-equalized').height('auto');

                if( !mqException || !Modernizr.mq(mqException) ){
                    $collection.each(function(i,e){ heightArray.push( $(e).outerHeight() ); });
                    $collection.css({ height : Math.max.apply( Math, heightArray ) }).addClass('height-equalized').attr('data-max-height', Math.max.apply( Math, heightArray ));
                }
            };

        setTimeout(function(){ eq_h( items ); }, 0);

        if( dinamic ) { 
            $window.on('resize', function(){ 
                setTimeout(function(){ eq_h( items ); }, 10);
            });
        }
    };

    $.fn.equalizeChildrenHeights = function( dinamic, mqException ){
        return this.each(function(i,e){
            if( $(e).parents('[data-override-eq="true"]').length ){ return; }
            $(e).children().equalizeHeights(dinamic, mqException);
        });
    };

    $.fn.equalizeTarget = function( dinamic, mqException ){
        return this.each(function( index, box ){
            $(box).find( $(box).data('eq-target') ).equalizeHeights( dinamic, mqException );
        });
    };

    $.fn.equalizeGroup = function( attname, dinamic, mqException ){
        var groups = this.groupByAtt( attname );

        groups.forEach(function( $set ){
            $set.equalizeHeights( dinamic, mqException );
        });

        return this;
    };

    $.fn.random = function() {
        var randomIndex = Math.floor(Math.random() * this.length);  
        return $(this[randomIndex]);
    };
}( this, jQuery ));


/////////////////////////////////////////
// Beautiful input plugin
/////////////////////////////////////////
(function( window, $, undefined ){
    window.Beautifier = function( element, callback ){
        this.$element = $(element);
        this.fileType = this.$element.data('file-type');
        this.name = this.$element.attr('name');
        this.placeholder = this.$element.data('placeholder');
        this.$fakeInput = $('<div />').attr({ 
            'class' : 'beautiful-input ' + this.fileType + (this.$element.data('aditional-classes') || ''), 
            'data-name' : this.name,
            'data-identifier' : this.$element.data('identifier') || 0
        }).text( this.placeholder || '' );

        this.$element.css({ 'position' : 'absolute', 'top' : '-999999em' });
        this.$element.after( this.$fakeInput );

        this.$fakeInput.on('click.beautify', { $realElement : this.$element }, function( event ){ 
            event.preventDefault(); 
            event.data.$realElement.trigger('click.beautify');  
        });

        this.$element.on('change.beautify',{ $fakeInput : this.$fakeInput }, function( event ){
            var input = event.currentTarget,
                $input = $(input);
            // si el elemento tiene definido un "max filesize" entonces se intenta enforzar
            if( $input.data('max-filesize') ){
                var maxSize = $input.data('max-filesize');

                // revisamos si se soporta la laecura de archivos
                if( typeof( input.files ) !== 'undefined'  ){
                    if( input.files[0] && input.files[0].size && input.files[0].size > maxSize ){
                        alert( 'El archivo sobrepasa el límite de tamaño, por favor selecione otro archivo.' );
                        return false;
                    }
                }
            }

            if( typeof( callback ) === 'function' ){
                callback( event );
            } else {
                var value = $input.val();
                value = value.replace("C:\\fakepath\\", '').replace("C:\/fakepath\/", '');
                
                event.data.$fakeInput.text( value ? value : $input.data('placeholder') ); 
            }
        });
    };
    $.fn.beautify = function( callback ) {
        return this.each(function() {
            var $element = $(this);
            if ($element.data('beautify')) { return $element.data('beautify'); }
            var beautify = new window.Beautifier( this, callback );
            $element.data('beautify', beautify);
        });
    };
}( this, jQuery ));


/////////////////////////////////////////
// Carga de tipografias
/////////////////////////////////////////

var familias = [ 
    'Roboto+Condensed:300i,300i,400,400i,500,500i,700,700i:latin',
    'Roboto:300,300i,400,400i,500,500i,700,700i:latin'
];

WebFontConfig = {
    google: { families: familias }
};

(function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = true;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
})();
