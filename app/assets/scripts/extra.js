$(document).on('change','[data-func="collapseForm"]',function(){
	if($(this).val() == 'empresa'){
		$('[data-form="empresa"]').fadeIn("fast");
		$('[data-form="particular"]').fadeOut("fast");
	}else{
		$('[data-form="empresa"]').fadeOut("fast");
		$('[data-form="particular"]').fadeIn("fast");
	}
});

$(document).on('change','[data-name="filtro-sence"]',function(){
    $('#filtrado-programas').submit();
});